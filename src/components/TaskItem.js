import React, { useState } from 'react';

function TaskItem({ task, deleteTask, toggleCompletion, editTask }) {
  const [editMode, setEditMode] = useState(false);
  const [editedText, setEditedText] = useState(task.text);

  const handleEditChange = (e) => {
    setEditedText(e.target.value);
  };

  const handleSaveEdit = () => {
    editTask(task.id, editedText);
    setEditMode(false);
  };

  return (
    <div className="task">
      <div className="content">
        <input
          type="checkbox"
          id={`checkbox-${task.id}`}
          name={`checkbox-${task.id}`}
          checked={task.completed}
          onChange={() => toggleCompletion(task.id)}
        />
        <label htmlFor={`checkbox-${task.id}`}>{task.text}</label>
      </div>
      <div className="actions">
        {editMode ? (
          <>
            <input
              type="text"
              className="text"
              value={editedText}
              onChange={handleEditChange}
            />
            <button onClick={handleSaveEdit}>Save</button>
          </>
        ) : (
          <>
            <button onClick={() => setEditMode(true)}>Edit</button>
            <button onClick={() => deleteTask(task.id)}>Delete</button>
          </>
        )}
      </div>
    </div>
  );
}

export default TaskItem;
