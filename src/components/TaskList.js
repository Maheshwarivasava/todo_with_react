import React from 'react';
import TaskItem from './TaskItem';

function TaskList({ todos, deleteTask, toggleCompletion, editTask }) {
  return (
    <main className="task-none">
      <section className="task-list">
        <h2>Tasks</h2>
        <div id="tasks">
          {todos.map((task) => (
            <TaskItem
              key={task.id}
              task={task}
              deleteTask={deleteTask}
              toggleCompletion={toggleCompletion}
              editTask={editTask}
            />
          ))}
        </div>
        <div className="task-footer">
          <div className="number-of-task">Tasks left: {todos.length}</div>
        </div>
      </section>
    </main>
  );
}

export default TaskList;
