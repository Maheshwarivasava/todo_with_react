import React, { useState } from 'react';

function TaskForm({ addTask }) {
  const [task, setTask] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    if (task.trim() !== '') {
      addTask(task);
      setTask('');
    }
  };

  return (
    <form id="new-task-form" onSubmit={handleSubmit}>
      <input
        type="text"
        name="new-task-input"
        id="new-task-input"
        placeholder="Plan your task.."
        value={task}
        onChange={(e) => setTask(e.target.value)}
      />
      <input type="submit" id="new-task-submit" value="Add task" />
    </form>
  );
}

export default TaskForm;
