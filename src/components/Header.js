import React from 'react';
import logoimage from '../images/Screenshot from 2024-03-06 12-13-15.png';

function Header() {
  return (
    <header>
      <h1>
        Todo List
        <img src={logoimage} alt="Logo" />
      </h1>
    </header>
  );
}

export default Header;
